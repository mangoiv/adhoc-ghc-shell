# adhoc-ghc-shell

drops you in a `nix develop` shell making some tooling I find useful available

Just run `nix develop "gitlab:mangoiv/adhoc-ghc-shell/main/#"` or `nix develop "gitlab:mangoiv/adhoc-ghc-shell/main/#ghc8"`
