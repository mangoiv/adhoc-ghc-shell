{
  description = "adhoc ghc devShell";
  nixConfig = {
    bash-prompt = "ghc ~ ";
    substituters = ["https://adhoc-ghc.cachix.org"];
    trusted-public-keys = ["adhoc-ghc.cachix.org-1:H60QeJCxnZFmsfDOXkfT/LwWmWdMSsDLPdl+IuL8ryA="];
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    old-nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
  };

  outputs = {
    self,
    nixpkgs,
    old-nixpkgs,
  }: let
    pkgsFor = system: import nixpkgs {inherit system;};
    oldPkgsFor = system: import old-nixpkgs {inherit system;};
    toolsFor = system: let
      pkgs = pkgsFor system;
    in [
      pkgs.cabal-install
      pkgs.haskellPackages.cabal-fmt
      pkgs.haskellPackages.fourmolu
      pkgs.hlint
    ];
    brokenPkgsFor = system:
      import nixpkgs {
        inherit system;
        config = {allowBroken = true;};
      };

    supportedSystems = ["x86_64-linux"];
    perSystem = nixpkgs.lib.genAttrs supportedSystems;

    agdaShellFor = system: ghcVersion: pkgFn: let
      pkgs = pkgFn system;
      aps = pkgs.agdaPackages;
      myAgda = pkgs.agda.withPackages {
        ghc = "ghc${ghcVersion}";
        pkgs = [
          aps.agda-categories
          aps.agda-prelude
          aps.agdarsec
          aps.standard-library
          aps.cubical
        ];
      };
    in
      pkgs.mkShell {
        buildInputs = [myAgda];
      };

    bareGHCFor = system: ghc: let
      pkgs = pkgsFor system;
      compiler-nix-name = "ghc${ghc}";
    in
      pkgs.mkShell {
        nativeBuildInputs =
          [
            pkgs.haskell.compiler.${compiler-nix-name}
          ]
          ++ (toolsFor system);
      };

    devShellFor = system: ghcVersion: pkgFn: let
      pkgs = pkgFn system;

      extraPkgs = ps:
        with ps;
          if (ghcVersion == "924" || ghcVersion == "942")
          then []
          else if (ghcVersion == "8107" && pkgs.config.allowBroken == true)
          then [
            # accelerate currently only compiles
            # with ghc8107 and is marked as broken
            accelerate
            accelerate-llvm
          ]
          else if (ghcVersion == "8107")
          then [
          ]
          else [];

      ghcPkgs = ps:
        with ps;
          [
            HTTP
            aeson
            barbies
            bytestring
            comonad
            constraints
            containers
            contravariant
            criterion
            effectful-core
            effectful
            fin
            first-class-families
            generics-sop
            interpolate
            HUnit
            lattices
            lens
            linear
            mtl
            profunctors
            recursion-schemes
            row-types
            row-types-aeson
            row-types-barbies
            singletons
            sop-core
            vec
            xmonad
            xmonad-contrib
          ]
          ++ extraPkgs ps;

      compiler-nix-name = "ghc" + ghcVersion;

      myGhc = pkgs.haskell.packages.${compiler-nix-name}.ghcWithPackages ghcPkgs;
      hls = pkgs.haskell-language-server.override {supportedGhcVersions = [ghcVersion];};
    in
      pkgs.mkShell {
        buildInputs =
          [
            myGhc
            pkgs.llvmPackages_9.libllvm
            pkgs.llvmPackages_9.llvm
            hls
          ]
          ++ (toolsFor system);
      };
  in rec {
    pkgs = perSystem pkgsFor;
    oldPkgs = perSystem oldPkgsFor;

    devShells = perSystem (system: rec {
      ghc9 = devShellFor system "924" pkgsFor;
      ghc9old = devShellFor system "902" pkgsFor;
      bare-ghc94 = bareGHCFor system "94";
      ghc8 = devShellFor system "8107" pkgsFor;
      accelerate = devShellFor system "8107" brokenPkgsFor;
      agda = agdaShellFor system "924" pkgsFor;
      default = ghc9;
    });

    checks = devShells;
    formatter = perSystem (system: let
      pkgs = pkgsFor system;
    in
      pkgs.alejandra);
  };
}
